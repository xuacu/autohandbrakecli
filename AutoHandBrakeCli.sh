#!/usr/bin/env bash

#################################################
# Hay que aplicar esto para que HandBrakeCLI no #
# escape del bucle for/while:                   #
# https://mywiki.wooledge.org/BashFAQ/089       #
#################################################
# Guía preset:                                  #
# https://forums.plex.tv/t/dvd-rip-quality-on-  #
# plex-roku-ultra/172481/12#Comment_1335697     #
#################################################
#
# Changelog
#   20201212
#	+ Añadido resume no interactivo
# Pendiente:
#	- Hacer el resume interactivo
#	- El resume no funciona si el nombre original tiene [ ] 

dpkg -l figlet &>/dev/null        || sudo apt install figlet -y 
dpkg -l handbrake-cli &>/dev/null || sudo apt install hadnbrake-cli -y
dpkg -l libdvd-pkg &>/dev/null    || sudo apt install libdvd-pkg -y || sudo dpkg-reconfigure libdvd-pkg

PRESET=DVD_a_H265_MKV_v4_nvenc

SOURCEDIR=$1
OUTDIR=$2

[[ -z "$SOURCEDIR" ]] && read -p 'Input dir: ' "SOURCEDIR" || echo $SOURCEDIR
[[ -z "$OUTDIR" ]] && read -p 'Output dir: ' "OUTDIR" || echo $OUTDIR

if [ -d "$SOURCEDIR" ]; then
       echo "" &>/dev/null
else
        echo "El directorio de entrada no existe o está mal: $OUTDIR " &&  exit;
fi

if [ -d "$OUTDIR" ]; then
	echo &>/dev/null
else
	echo "El directorio de salida no existe o está mal: $OUTDIR " &&  exit;
fi

##### R E S U M E #####################
# [[ -f "${OUTDIR%/}"/.resume ]] || echo starting && echo hola
# find ${SOURCEDIR%/} -name VIDEO_TS -type d ! -path "$(awk '{ print "\""$0"\""}' .resume)" | sort | uniq | grep -v "$(cat .resume)"
#######################################

NOTIFY=$(which notify-send)

clean() {
	trap 'rm "$NAME.$TMP" "$TMP"' EXIT;
}

export -f clean
find "${SOURCEDIR%/}" -iname VIDEO_TS -exec echo {} \; | sort

[[ ! -f "${SOURCEDIR%/}"/.resume ]] || echo skipping $(cat "${SOURCEDIR%/}"/.resume 2>/dev/null)

if [ -f "${SOURCEDIR%/}"/.resume ]; then
  find "${SOURCEDIR%/}" -iname VIDEO_TS | grep -v "$(cat "$SOURCEDIR"/.resume)" | sort | while read item
   do
    declare -t  NAME=$(echo "$item" | awk -F "/" '{print $(NF-1)}' | sed 's/^\.//g')
    declare -t  TMP=$(mktemp /tmp/.TMP.XXXXXXX);
    figlet "$NAME"
    [[ -z NOTIFY ]] || notify-send --app-name=HandBrake "$NAME"
    clean
    HandBrakeCLI -i "$(echo "$item")" -t 0 2> "$TMP" </dev/null
    for title in $(grep "+ title" "$TMP" | awk '{ print $3 }' | grep -o '[0-9]\+')
      do
        HandBrakeCLI -i "$item" -o "${OUTDIR%/}/$NAME$title.mkv" -t $title --all-audio --subtitle "$(seq -s, 1 20)" --preset-import-file $PWD/*.json -Z "$PRESET" </dev/null 2> /dev/null
  	    du -h "$OUTDIR"/"$(basename "$(ls -tra "${OUTDIR%/}"/"$NAME"*.mkv | tail -n1)")"
        du -h "$OUTDIR"/"$(basename "$(ls -tra "${OUTDIR%/}"/"$NAME"*.mkv | tail -n1)")" >> $OUTDIR/"$(date +%Y%m%d)".log
        echo "$item" >> "${SOURCEDIR%/}"/.resume
      done
   done && rm "${SOURCEDIR%/}"/.resume
else
  find "${SOURCEDIR%/}" -iname VIDEO_TS | sort | while read item
   do
    declare -t  NAME=$(echo "$item" | awk -F "/" '{print $(NF-1)}' | sed 's/^\.//g')
    declare -t  TMP=$(mktemp /tmp/.TMP.XXXXXXX);
    figlet "$NAME"
    [[ -z NOTIFY ]] || notify-send --app-name=HandBrake "$NAME"
    clean
    HandBrakeCLI -i "$(echo "$item")" -t 0 2> "$TMP" </dev/null
    for title in $(grep "+ title" "$TMP" | awk '{ print $3 }' | grep -o '[0-9]\+')
      do
        HandBrakeCLI -i "$item" -o "${OUTDIR%/}/$NAME$title.mkv" -t $title --all-audio --subtitle "$(seq -s, 1 20)" --preset-import-file $PWD/*.json -Z "$PRESET" </dev/null 2> /dev/null
  	    du -h "$OUTDIR"/"$(basename "$(ls -tra "${OUTDIR%/}"/"$NAME"*.mkv | tail -n1)")"
        du -h "$OUTDIR"/"$(basename "$(ls -tra "${OUTDIR%/}"/"$NAME"*.mkv | tail -n1)")" >> $OUTDIR/$(date +%Y%m%d).log
        echo "$item" >> "${SOURCEDIR%/}"/.resume
      done
   done && rm "${SOURCEDIR%/}"/.resume
fi
