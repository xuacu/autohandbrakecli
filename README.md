# AutoHandBrakeCli

Automatiza el ripeo de DVDs a MKV con máxima calidad.

## Uso
Básicamente hay que ejecutar el script e indicarle un directorio (Input dir) que contendrá uno o varios DVDs. El script compondrá una lista ordenada alfabéticamente e irá ripeándolos todos y extrayendo **TODO** el contenido al mismo directorio de entrada.

## Requerimientos
- HandBrakeCli +v1.2.1
- GPU Nvidia*
- figlet
- el archivo json con el preset

\* *en caso de no querer/poder usar la GPU hay que cambiar en el json el parámetro "VideoEncoder": "nvenc_h265", por "VideoEncoder": "h265",. Para mi el rendimiento varía de 12fps usando el procesador a **200+fps** usando GPU*

